from datetime import datetime

from django.http import HttpResponse
from django.shortcuts import render


def welcome(request):
    # return HttpResponse("Welcome to the My Meeting Planner, geeks!")
    return render(request, "website/welcome.html")


def date(request):
    return HttpResponse("Date" + str(datetime.now()))
