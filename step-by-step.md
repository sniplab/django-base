# Django: Getting Started [Pluralsight]

https://github.com/codesensei-courses/django_getting_started

1.
```
pip install django
django-admin startproject meeting_planner
cd meeting_planner
python manage.py runserver
```

## Core config file for Django
meeting_planner/settings.py

2.
- `python manage.py startapp website`
- add website to INSTALLED_APPS (settings.py)
- create view function/handler  ("def welcome(request)")
- add url to urls.py
- mark meeting_planner as "Source root" directory
- `python manage.py runserver`
- go to http://127.0.0.1:8000/welcome.html

fix error:

# NG
```
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': BASE_DIR / 'db.sqlite3',
    }
}
```

# OK: add str()
```
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': str(BASE_DIR / "db.sqlite3"),
    }
}
```

3. DB 
   (Django has native support: PostgreSQL, MariaDB, MySQL, Oracle, SQLite;
   with packages: DB2, MS SQL, etc.)
```
python manage.py showmigrations
python manage.py migrate
python manage.py dbshell (sqlite sh!)
    .tables
    select * from django_migrations;
```

4. create app "meetings"

```
python manage.py startapp meetings
add website to INSTALLED_APPS (settings.py)
create `class Meeting(models.Model)`
python manage.py makemigrations (add/change db froom models changes)
python manage.py sqlmigrate meetings 0001
```

```sqlite
CREATE TABLE "meetings_meeting" 
("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "title" varchar(200) NOT NULL, "date" date NOT NULL);
```

```
python manage.py migrate
python manage.py dbshell
    .tables >>> we will see  NEW `meetings_meeting` table
    .exit
```

4. admin.py
```
from .models import Meeting
admin.site.register(Meeting)
python manage.py createsuperuser  # admin user
   faith
   faith@gmail.com
   12345
```

5. db models

```
this is a sort of class
__str__ FOR ADMIN INTERGACE
foreign key: `room = models.ForeignKey(Room, on_delete=models.CASCADE)`
```

5. MTV (Model-Template-View)
